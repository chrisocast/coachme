'use strict';

var express = require('express');
var router = express.Router();
var url = require('url');
var pg = require('pg');
var connectionString = process.env.DATABASE_URL;
var squel = require("squel");

/**
 * @api {get} /user/:coach_id Get month of appointments for one coach
 * @apiName GetAppointmentsByCoach
 * @apiGroup AppointmentCalendar
 *
 * @apiParam {Number} coach_id  Coach's unique ID.
 * @apiParam {Number} year Optional Year to use for appointment query.
 * @apiParam {Number} month Optional  Month to use for appointment query (not zero based).
 *
 * @apiSuccess {Array} appointments List of appointment objects.
 * @apiSuccess {Object} appointment Individual appointment object.
 */

router.get('/:coach_id', function(req, res, next) {
  var results = [];

  var coach_id = req.params.coach_id;
  var urlquery = url.parse(req.url, true);

  var now = new Date();
  var queryYear = urlquery.query.year || now.getFullYear();
  var queryMonth = urlquery.query.month || (now.getMonth() + 1);
  var queryDate = new Date(queryYear, queryMonth);

  pg.connect(connectionString, function(err, client, done) {
    
    var query = client.query(buildMonthApptQuery(coach_id, queryDate));

    query.on('row', function(row) {
      results.push(row);
    });

    query.on('end', function() {
      client.end();
      return res.json(results);
    });

    if(err) {
      console.log("Error getting appointments. ", err);
    }

  });
});

/**
 * @api {post} /add Create new appointment
 * @apiName CreateAppointment
 * @apiGroup AppointmentCalendar
 *
 * @apiParam {Timestamp} start_time  Appointment start time format with timezone info.
 * @apiParam {Boolean} canceled  Coach's unique ID.
 * @apiParam {Number} coach_id  Coach's unique ID.
 * @apiParam {Number} client_id  Client's unique ID.
 * @apiParam {Boolean} send_email_confirmation  Whether user wants an email confirming the appointment was created.
 * @apiParam {Boolean} send_email_reminder Whether user wants an email reminder before the appointment.
 *
 * @apiSuccess {Array} appointments List of appointment objects.
 * @apiSuccess {Object} appointment Individual appointment object.
 */
router.post('/add', function(req, res) {

    var results = [];

    var data = {
      start_time: req.body.start_time,
      canceled: req.body.canceled || false,
      coach_id: req.body.coach_id,
      client_id: req.body.client_id,
      send_email_confirmation: req.body.send_email_confirmation,
      send_email_reminder: req.body.send_email_reminder
    };

    pg.connect(connectionString, function(err, client, done) {

        var sqlQuery = squel.insert()
          .into("appointments")
          .set("start_time", data.start_time)
          .set("canceled", data.canceled)
          .set("coach_id", data.coach_id)
          .set("client_id", data.client_id)
          .set("send_email_confirmation", data.send_email_confirmation)
          .set("send_email_reminder", data.send_email_reminder);

        client.query(sqlQuery.toString());
        
        var returnQuery = squel.select()
          .from("appointments")
          .where("coach_id = " + data.coach_id)
          .where("client_id = " + data.client_id)
          .where("canceled <> true")
          .where("start_time = '" + data.start_time + "'");

        var query = client.query(returnQuery.toString());

        query.on('row', function(row) {
            results.push(row);
        });

        query.on('end', function() {
            client.end();
            return res.json(results);
        });

        // Handle Errors
        if(err) {
          console.log("Error adding item. ", err);
        }

    });
});

/**
 * @api {put} /update/:appt_id Update existing appointment
 * @apiName UpdateAppointment
 * @apiGroup AppointmentCalendar
 *
 * @apiParam {Number} appt_id  Appointment's unique ID.
 * @apiParam {Timestamp} start_time  Appointment start time format with timezone info.
 * @apiParam {Boolean} canceled  Coach's unique ID.
 * @apiParam {Number} coach_id  Coach's unique ID.
 * @apiParam {Number} client_id  Client's unique ID.
 * @apiParam {Boolean} send_email_confirmation  Whether user wants an email confirming the appointment was created.
 * @apiParam {Boolean} send_email_reminder Whether user wants an email reminder before the appointment.
 *
 * @apiSuccess {Array} appointments List of appointment objects.
 * @apiSuccess {Object} appointment Individual appointment object.
 */
 router.put('/update/:appt_id', function(req, res) {

    var results = [];
    var id = req.params.appt_id;
    var data = {
      start_time: req.body.start_time,
      canceled: req.body.canceled || false,
      coach_id: req.body.coach_id,
      client_id: req.body.client_id,
      send_email_confirmation: req.body.send_email_confirmation || false,
      send_email_reminder: req.body.send_email_reminder || false
    };

    pg.connect(connectionString, function(err, client, done) {
        
        var sqlQuery = squel.update()
          .table("appointments")
          .set("start_time", data.start_time)
          .set("canceled", data.canceled)
          .set("coach_id", data.coach_id)
          .set("client_id", data.client_id)
          .set("send_email_confirmation", data.send_email_confirmation)
          .set("send_email_reminder", data.send_email_reminder)
          .where("id = " + id);

        client.query(sqlQuery.toString());
        
        var apptDate = new Date(data.start_time);
        var query = client.query(buildMonthApptQuery(data.coach_id, apptDate));

        query.on('row', function(row) {
            results.push(row);
        });

        query.on('end', function() {
            client.end();
            return res.json(results);
        });

        if(err) {
          console.log("Error updating item. ", err);
        }
    });
});

// Constructs query for one month of appointments for coach
function buildMonthApptQuery(coach_id, date){
  var timestamp = date.getFullYear() + '-' + date.getMonth() + '-01';

  var sqlQuery = squel.select()
  .from("appointments")
  .where("coach_id = " + coach_id)
  .where("canceled = false OR canceled = null")
  .where("start_time >= date_trunc('month', date '"+timestamp+"')")
  .where("start_time < (date_trunc('MONTH', date '"+timestamp+"') + INTERVAL '1 MONTH')")
  .order("id");

  return sqlQuery.toString();
}

module.exports = router;
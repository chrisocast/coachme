'use strict';

var express = require('express');
var router = express.Router();
var pg = require('pg');
var connectionString = process.env.DATABASE_URL;

router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});

router.get('/appointment-calendar', function(req, res, next) {
  res.render('calendar', { title: 'Express' });
});

module.exports = router;

--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: appointments; Type: TABLE; Schema: public; Owner: j-chca; Tablespace: 
--

CREATE TABLE appointments (
    id integer NOT NULL,
    client_id integer NOT NULL,
    coach_id integer NOT NULL,
    start_time timestamp with time zone NOT NULL,
    coach_notes text,
    canceled boolean DEFAULT false,
    send_email_confirmation boolean DEFAULT false,
    send_email_reminder boolean DEFAULT false
);


ALTER TABLE appointments OWNER TO "j-chca";

--
-- Name: appointments_id_seq; Type: SEQUENCE; Schema: public; Owner: j-chca
--

CREATE SEQUENCE appointments_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE appointments_id_seq OWNER TO "j-chca";

--
-- Name: appointments_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: j-chca
--

ALTER SEQUENCE appointments_id_seq OWNED BY appointments.id;


--
-- Name: clients; Type: TABLE; Schema: public; Owner: j-chca; Tablespace: 
--

CREATE TABLE clients (
    id integer NOT NULL,
    first_name text NOT NULL,
    last_name text NOT NULL,
    email text,
    coach_id integer,
    username text
);


ALTER TABLE clients OWNER TO "j-chca";

--
-- Name: clients_id_seq; Type: SEQUENCE; Schema: public; Owner: j-chca
--

CREATE SEQUENCE clients_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE clients_id_seq OWNER TO "j-chca";

--
-- Name: clients_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: j-chca
--

ALTER SEQUENCE clients_id_seq OWNED BY clients.id;


--
-- Name: coaches; Type: TABLE; Schema: public; Owner: j-chca; Tablespace: 
--

CREATE TABLE coaches (
    id integer NOT NULL,
    first_name text NOT NULL,
    last_name text NOT NULL,
    email text NOT NULL,
    username text NOT NULL,
    workday_start timestamp without time zone,
    workday_end timestamp without time zone,
    avatar_url text,
    bio text
);


ALTER TABLE coaches OWNER TO "j-chca";

--
-- Name: coaches_id_seq; Type: SEQUENCE; Schema: public; Owner: j-chca
--

CREATE SEQUENCE coaches_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE coaches_id_seq OWNER TO "j-chca";

--
-- Name: coaches_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: j-chca
--

ALTER SEQUENCE coaches_id_seq OWNED BY coaches.id;


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: j-chca
--

ALTER TABLE ONLY appointments ALTER COLUMN id SET DEFAULT nextval('appointments_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: j-chca
--

ALTER TABLE ONLY clients ALTER COLUMN id SET DEFAULT nextval('clients_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: j-chca
--

ALTER TABLE ONLY coaches ALTER COLUMN id SET DEFAULT nextval('coaches_id_seq'::regclass);


--
-- Data for Name: appointments; Type: TABLE DATA; Schema: public; Owner: j-chca
--

COPY appointments (id, client_id, coach_id, start_time, coach_notes, canceled, send_email_confirmation, send_email_reminder) FROM stdin;
68	5	2	2015-09-08 08:00:00-07	\N	f	f	f
73	5	2	2015-09-16 08:00:00-07	\N	f	f	f
78	5	2	2015-09-23 08:00:00-07	\N	f	f	f
83	5	2	2015-09-21 10:00:00-07	\N	f	f	f
88	5	2	2015-09-11 14:00:00-07	\N	f	f	f
65	1	2	2015-09-01 09:00:00-07	\N	t	f	f
70	1	2	2015-09-01 10:00:00-07	\N	f	f	f
75	1	2	2015-09-17 10:00:00-07	\N	f	f	f
80	1	2	2015-09-15 08:00:00-07	\N	f	f	f
85	1	2	2015-09-22 10:00:00-07	\N	f	f	f
90	1	2	2015-09-30 10:00:00-07	\N	f	f	f
69	6	2	2015-09-10 09:00:00-07	\N	f	t	f
74	6	2	2015-09-16 10:00:00-07	\N	f	f	f
79	6	2	2015-09-16 15:00:00-07	\N	f	f	f
84	6	2	2015-09-21 14:00:00-07	\N	f	f	f
89	6	2	2015-09-30 09:00:00-07	\N	f	f	f
67	4	2	2015-09-09 14:00:00-07	\N	f	t	t
72	4	2	2015-09-15 14:00:00-07	\N	f	f	f
77	4	2	2015-09-24 10:00:00-07	\N	f	f	f
82	4	2	2015-09-21 08:00:00-07	\N	f	f	f
87	4	2	2015-09-11 10:00:00-07	\N	f	f	f
66	2	2	2015-09-01 08:00:00-07	\N	t	f	f
71	2	2	2015-09-14 08:00:00-07	\N	f	f	f
76	2	2	2015-09-18 14:00:00-07	\N	f	f	f
81	2	2	2015-09-14 14:00:00-07	\N	f	f	f
86	2	2	2015-09-25 09:00:00-07	\N	f	f	f
91	2	2	2015-09-29 08:00:00-07	\N	f	f	f
93	5	2	2015-09-29 15:00:00-07	\N	f	f	f
98	5	2	2015-09-17 14:00:00-07	\N	f	f	f
103	5	2	2015-09-24 14:00:00-07	\N	f	f	f
108	5	2	2015-09-08 09:00:00-07	\N	f	f	f
95	1	2	2015-09-28 14:00:00-07	\N	f	f	f
100	1	2	2015-09-04 14:00:00-07	\N	f	f	f
105	1	2	2015-09-22 09:00:00-07	\N	f	f	f
94	6	2	2015-09-28 08:00:00-07	\N	f	f	f
99	6	2	2015-09-18 08:00:00-07	\N	f	f	f
104	6	2	2015-09-24 15:00:00-07	\N	f	f	f
109	6	2	2015-09-08 14:00:00-07	\N	f	f	f
92	4	2	2015-09-29 09:00:00-07	\N	f	f	f
97	4	2	2015-09-24 08:00:00-07	\N	f	f	f
102	4	2	2015-09-24 09:00:00-07	\N	f	f	f
107	4	2	2015-09-18 10:00:00-07	\N	f	f	f
96	2	2	2015-09-23 10:00:00-07	\N	f	f	f
101	2	2	2015-09-25 14:00:00-07	\N	f	f	f
106	2	2	2015-09-16 09:00:00-07	\N	f	f	f
110	3	2	2015-09-03 14:00:00-07	\N	f	f	f
64	3	2	2015-09-16 09:00:00-07	\N	f	f	f
\.


--
-- Name: appointments_id_seq; Type: SEQUENCE SET; Schema: public; Owner: j-chca
--

SELECT pg_catalog.setval('appointments_id_seq', 110, true);


--
-- Data for Name: clients; Type: TABLE DATA; Schema: public; Owner: j-chca
--

COPY clients (id, first_name, last_name, email, coach_id, username) FROM stdin;
1	Robert	Ellis	robert.ellis@gmail.com	1	rellis
3	George	Hernandez	ghernandez22@gmail.com	2	ghernandez
2	Lisa	Swanson	lisas@yahoo.com	1	lisas
4	Neil	Rassmuson	neilguy@comcast.net	3	neilhealth
5	Lucy	White	lucy332@hotmail.com	2	lucyw
6	Peter	Bookman	peterbook@gmail.com	3	pbook
\.


--
-- Name: clients_id_seq; Type: SEQUENCE SET; Schema: public; Owner: j-chca
--

SELECT pg_catalog.setval('clients_id_seq', 1, false);


--
-- Data for Name: coaches; Type: TABLE DATA; Schema: public; Owner: j-chca
--

COPY coaches (id, first_name, last_name, email, username, workday_start, workday_end, avatar_url, bio) FROM stdin;
2	Alex	Nelson	alex.nelson@arivale.com	alex.nelson	\N	\N	\N	\N
1	Ella	McKinley	ella.mckinley@arivale.com	ella.mckinley	\N	\N	\N	\N
3	David	Larson	david.larson@arivale.com	david.larson	\N	\N	\N	\N
\.


--
-- Name: coaches_id_seq; Type: SEQUENCE SET; Schema: public; Owner: j-chca
--

SELECT pg_catalog.setval('coaches_id_seq', 1, false);


--
-- Name: appointments_pkey; Type: CONSTRAINT; Schema: public; Owner: j-chca; Tablespace: 
--

ALTER TABLE ONLY appointments
    ADD CONSTRAINT appointments_pkey PRIMARY KEY (id);


--
-- Name: clients_pkey; Type: CONSTRAINT; Schema: public; Owner: j-chca; Tablespace: 
--

ALTER TABLE ONLY clients
    ADD CONSTRAINT clients_pkey PRIMARY KEY (id);


--
-- Name: coaches_pkey; Type: CONSTRAINT; Schema: public; Owner: j-chca; Tablespace: 
--

ALTER TABLE ONLY coaches
    ADD CONSTRAINT coaches_pkey PRIMARY KEY (id);


--
-- Name: appointments_clientid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: j-chca
--

ALTER TABLE ONLY appointments
    ADD CONSTRAINT appointments_clientid_fkey FOREIGN KEY (client_id) REFERENCES clients(id);


--
-- Name: appointments_coachid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: j-chca
--

ALTER TABLE ONLY appointments
    ADD CONSTRAINT appointments_coachid_fkey FOREIGN KEY (coach_id) REFERENCES coaches(id);


--
-- Name: clients_coachid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: j-chca
--

ALTER TABLE ONLY clients
    ADD CONSTRAINT clients_coachid_fkey FOREIGN KEY (coach_id) REFERENCES coaches(id);


--
-- Name: public; Type: ACL; Schema: -; Owner: j-chca
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM "j-chca";
GRANT ALL ON SCHEMA public TO "j-chca";
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- PostgreSQL database dump complete
--


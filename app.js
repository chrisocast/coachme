var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var exphbs  = require('express-handlebars');

var index = require('./server/routes/main');
var api = require('./server/routes/api');

var app = express();

app.engine('.hbs', exphbs({
  defaultLayout: 'layout',
  defaultLayout: path.join(__dirname, './views/layouts/main'),
  layoutsDir: path.join(__dirname, './views/layouts/'),
  partialsDir: path.join(__dirname, './views/partials/'),
  extname: '.hbs'
}));

app.set('port', process.env.PORT);
app.set('view engine', '.hbs');

app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'client')));

if (process.env.NODE_ENV === 'development') {
  var livereload = require('connect-livereload')();
  app.use(livereload);

  app.use('/styles', express.static(path.join(__dirname, '/dist/styles')));
  app.use('/scripts', express.static(path.join(__dirname, '/dist/scripts')));
  app.use('/img', express.static(path.join(__dirname, '/dist/img')));
  app.use('/fonts', express.static(path.join(__dirname, '/dist/fonts')));
}

app.use('/', index);
app.use('/api/appointments', api);
app.use('/api/appointments/update', api);
app.use('/api/appointments/add', api);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
  app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
      message: err.message,
      error: err
    });
  });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
  res.status(err.status || 500);
  res.render('error', {
    message: err.message,
    error: {}
  });
});

app.listen(app.get('port'), function() {
  console.log('Server running on port ' + app.get('port'));
});

module.exports = app;

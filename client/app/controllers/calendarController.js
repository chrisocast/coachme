angular.module('CalendarController', ['calendarService'])
  .controller('CalendarController', ['calendarService', '$scope', '$http', function(calService, $scope, $http) {
	var currDate = new Date();
	var self = this;

	// Set scope vars
	$scope.clientId = 3;
	$scope.coachId = 2;
	$scope.apptViewVisible = false;
	$scope.apptView = {};
	$scope.checkboxModel = {
		value1 : false,
		value2 : false
	};
	
	$http.get('/api/appointments/' + $scope.coachId)
		.then(function(res) {
			// Store the data within scope
			$scope.apptData = res.data;

			// Construct calendar model
			$scope.dates = calService.getMonthDateList(currDate);
			self.addApptsToCalendar($scope.apptData);
			$scope.dates = calService.addPrevAndNextMonthDays($scope.dates, currDate);

		}, function(response) {
			console.log("Error loading appt data");
		});

	this.addApptsToCalendar = function(){
		var datesLen = $scope.dates.length;
		var apptsLen = $scope.apptData.length;

		for (var i = 0; i < apptsLen; i++) {
			var apptDay = new Date($scope.apptData[i].start_time).getDate()-1;
			var apptTimeMs = new Date($scope.apptData[i].start_time).getTime();

			for (var k = 0; k < $scope.dates[apptDay].timeSlots.length; k++) {

				if (apptTimeMs === new Date($scope.dates[apptDay].timeSlots[k].start_time).getTime()){
					var ts = $scope.dates[apptDay].timeSlots[k];
					$scope.dates[apptDay].timeSlots[k] = $scope.apptData[i];
					$scope.dates[apptDay].timeSlots[k].display_time = moment(new Date($scope.apptData[i].start_time)).format('h a');
					this.setTimeSlotStatus($scope.dates[apptDay].timeSlots[k], $scope.clientId);
					break;
				}
			}
		}
	};

	this.setTimeSlotStatus = function(timeSlot, active_client_id){
		if (timeSlot.client_id === active_client_id){
			timeSlot.status = 'scheduled'; // has been scheduled by current client
			timeSlot.message = 'Scheduled';
		} else {
			timeSlot.status = 'closed'; // has been scheduled by another client
		}
	};

	this.openApptView = function(timeSlot){
		if(timeSlot.status !== 'closed'){
			var apptStartDate = new Date(timeSlot.start_time);

			$scope.apptView.displayDate = moment(apptStartDate).format('dddd, MMMM D, YYYY');
			$scope.apptView.displayTime = moment(apptStartDate).format('h:mm a');
			$scope.apptView.startDate = apptStartDate;
			$scope.apptView.timeSlot = timeSlot;
			$scope.apptViewVisible = true;

			if (timeSlot.status === 'open'){
				$scope.apptView.title = "Schedule an appointment";
			} else {
				$scope.apptView.title = "Your appointment";
			}
		}
	};

	this.closeApptView = function(){
		$scope.apptViewVisible = false;
		self.resetApptView();
	};

	this.resetApptView = function(){
		$scope.apptView = {};
		$scope.checkboxModel.value1=false;
		$scope.checkboxModel.value2=false;
	};

	this.addAppt = function(apptView){

		var apptData = {
			start_time: new Date(apptView.startDate),
			coach_id: $scope.coachId,
			client_id: $scope.clientId,
			send_email_confirmation: $scope.checkboxModel.value1,
			send_email_reminder: $scope.checkboxModel.value2
	    };

		$http.post('/api/appointments/add', apptData)
			.then(function(res) {
				apptView.timeSlot.id = res.data[0].id;
				apptView.timeSlot.status = 'scheduled';
				apptView.timeSlot.message = 'Scheduled';
			}, function(res) {
				apptView.timeSlot.status = 'error';
				apptView.timeSlot.message = 'Error';
			});

		$scope.apptViewVisible = false;
		self.resetApptView();
	};

	this.cancelAppt = function(apptView){
		apptView.timeSlot.canceled = true;
		self.updateAppt(apptView);
	};

	this.updateAppt = function(apptView){
	
	    var apptData = {
			start_time: new Date(apptView.startDate),
			coach_id: $scope.coachId,
			client_id: $scope.clientId,
			send_email_confirmation: $scope.checkboxModel.value1,
			send_email_reminder: $scope.checkboxModel.value2,
			canceled: apptView.timeSlot.canceled
	    };

		$http.put('/api/appointments/update/' + apptView.timeSlot.id, apptData)
			.then(function(res) {
				apptView.timeSlot.id = null;
				apptView.timeSlot.status = 'open';
				apptView.timeSlot.message = '';
			}, function(res) {
				apptView.timeSlot.status = 'error';
				apptView.timeSlot.message = 'Error';
			});

		$scope.apptViewVisible = false;
		self.resetApptView();
	};
}])
.directive('appt-overlay', function() {
  return {
  	restrict: 'E',
    templateUrl: function(){
      return '/app/views/appt-overlay.html';
    }
  };
});
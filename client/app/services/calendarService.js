angular.module('calendarService', [])
	.factory('calendarService', function() {

	var getMonthDateList = function(anyDateInMonth){

		var list = [];
		var daysInMonth = new Date(anyDateInMonth.getFullYear(), anyDateInMonth.getMonth()+1, 0).getDate();
		
		// Add curr month days
		for (var i = daysInMonth; i >= 1; i--) {
			var itemDate = new Date(anyDateInMonth.getFullYear() + ' ' + (anyDateInMonth.getMonth() + 1) + ' ' + i);

			if (itemDate.getDay() === 6 || itemDate.getDay() === 0){
				list.unshift({
					date_num: i,
					timestamp: null,
					status: 'blocked',
					timeSlots: [] 
				});
			} else {
				list.unshift({
					date_num: i,
					timestamp: null,
					status: 'open',
					timeSlots: buildTimeSlots(anyDateInMonth, i)
			    });
			}
		};

		return list;
	};

	var buildTimeSlots = function(anyDateInMonth, day){
		var yr = anyDateInMonth.getFullYear();
		var mo = anyDateInMonth.getMonth() + 1;
		
		return [
				{ start_time: buildUTCDate(yr,mo,day,15), display_time: '8am', status: 'open'},
				{ start_time: buildUTCDate(yr,mo,day,16), display_time: '9am', status: 'open'},
				{ start_time: buildUTCDate(yr,mo,day,17), display_time: '10am', status: 'open'},
				{ start_time: buildUTCDate(yr,mo,day,21), display_time: '2pm', status: 'open'},
				{ start_time: buildUTCDate(yr,mo,day,22), display_time: '3pm', status: 'open'}
			];
	};

	var buildUTCDate = function(year, month, day, hour){
		return new Date(year + '-' + month + '-' + day + ' ' + hour + ':00 Z');
	};

	var addPrevAndNextMonthDays = function(dateList, anyDateInMonth){
		var firstDateInMonth = new Date(anyDateInMonth.getFullYear(), anyDateInMonth.getMonth(), 1);
		var firstDayOfMonth = firstDateInMonth.getDay();  // 0=Sunday, 1=Monday, etc

		// Add prev month days
		if (firstDayOfMonth !== 0){
			for (var j = firstDayOfMonth; j >= 1; j--) {
				dateList.unshift({status: 'blocked'});
			};
		}

		// Add next month days
		if (dateList.length < 35){
			var daysToAdd = 35 - dateList.length;
			for (var k = daysToAdd; k >= 1; k--) {
				dateList.push({status: 'blocked'});
			};
		}

		return dateList;
	};

	return {
    	getMonthDateList: getMonthDateList,
    	addPrevAndNextMonthDays: addPrevAndNextMonthDays
	};
});
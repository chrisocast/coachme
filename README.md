### CoachMe Appointment Calendar

**To run the app locally:**
- Clone the [repo](https://gitlab.com/chrisocast/coachme) or use just use project folder sent via email. The repo is currently in Gitlab because it offers free private repos.
- Run ``npm install``
- Spin up a local postgres db:
    - Backup of the db for restoring can be found in project: ``server/database/backup.sql``
    - The url to the database is set as an env var in the Gruntfile.js. It is currently: ``postgres://localhost:5432/coachme``
- Run ``grunt``
- Go to http://localhost:9001/appointment-calendar
- To test the example prod build process, run ``grunt prod-build``

**Assumptions were made**
- Since appointments are one hour long, coaches won't have more than five a day. Leaving three hours for internal meetings, prep time, etc.
- I picked 8am, 9am, 10am, 2pm, and 3pm as the standard times when appointments can be scheduled. This should be customizeable by coach (see 'potential improvements' below).
- No appointments on weekends

**Potential improvements**
- I built the basic javascript calendar view. It's small and seems ok so far, but an OS solution that I could plug-n-play might be more tested/bulletproof.
- I started creating date filters to allow a user to hide all Monday appointments or anything before 10am. Would be a very useful feature.
- In Angular, the app structure could be better. More directives with templates for example to break up the responsibilities.
- A login system would be required to properly inform the app how to construct their queries.
- This client focused calendar could be easily re-used to provide coaches with their own personal view for looking through all appointments. Hooking up to Outlook would be great also. 
- Coaches should be able to 'black out' dates based on vacation or other conflicts.
module.exports = function (grunt) {
  require('load-grunt-tasks')(grunt);
  require('time-grunt')(grunt);

  grunt.initConfig({
    express: {
      dev: {
        options: {
          port: 9001,
          script: './app.js',
          spawn: false
        }
      }
    },

    watch: {
      express: {
        files: ['./app.js', 'server/routes/*.js'],
        tasks: ['express'],
        options: {
          livereload: true
        }
      },
      scss: {
        files: 'client/styles/**/*.scss',
        tasks: ['sass:dev'],
        options: {
          livereload: true
        }
      },
      js: {
        files: 'client/app/**/*.js',
        tasks: [
          'concat:dev'
        ],
        options: {
          livereload: true
        }
      },
      views: {
        files: ['views/**/*.hbs'],
        options: {
          livereload: true
        }
      }
    },

    concat: {
      dev: {
        options: {
          sourceMap: true
        },
        src: [
          'node_modules/angular/angular.js',
          'node_modules/moment/min/moment.min.js',
          'client/app/**/*.js'
        ],
        dest: 'dist/scripts/main.js',
      },
      prod: {
        src: [
          'node_modules/angular/angular.js',
          'node_modules/moment/min/moment.min.js',
          'client/app/**/*.js'
        ],
        dest: 'dist/scripts/main.js',
      },
    },

    sass: {
      dev: {
        files: [{
          expand: true,
          cwd: 'client/styles',
          src: ['**/*.scss'],
          dest: 'dist/styles',
          ext: '.css'
        }],
        options: {
          outputStyle: 'expanded'
        }
      },
      prod: {
        files: [{
          expand: true,
          cwd: 'client/styles',
          src: ['**/*.scss'],
          dest: 'dist/styles',
          ext: '.css'
        }],
        options: {
          outputStyle: 'compressed'
        }
      }
    },

    jshint: {
      all: ['server/bin/www',
            'server/**/*.js',
            'client/app/**/*.js'
            ],
      options: {
        curly: true,
        eqeqeq: true,
        immed: true,
        latedef: 'nofunc',
        undef: false,
        unused: false,
        laxbreak: true,
        globals: {
          jQuery: true,
          require: true
        },
        devel: {
          console: true
        }
      }
    },

    env: {
      dev: {
        NODE_ENV : 'development',
        API_BASE_PATH: 'http://localhost:9001/api/appointments',
        DATABASE_URL: 'postgres://localhost:5432/coachme',
        PORT: 9001
      },
      prod: {
        NODE_ENV : 'production',
        API_BASE_PATH: 'http://localhost:9001/api/appointments',
        DATABASE_URL: 'postgres://localhost:5432/coachme',
        PORT: 9001
      }
    },

    clean: {
      dist: {
        files: [
          { src: ['dist'] }
        ]
      }
    },

    copy: {
      main: {
        "files": [
          { "cwd": "client/", "src": ["img/**"], "dest": "dist/", "expand": true },
          { "src": ["views/**"], "dest": "dist/", "expand": true },
          { "cwd": "server/", "src": ["routes/**"], "dest": "dist/", "expand": true }
        ]
      }
    },

    apidoc: {
      myapp: {
        src: "server/routes/",
        dest: "dist/apidoc/"
      }
    },

    exec: {
      backup_db: {
        command: 'echo "Starting db dump..." && pg_dump coachme > server/database/backup.sql && echo "Completed db dump"'
      },
      restore_db: {
        command: 'echo "Starting db restore from backup..." && psql coachme < server/database/backup.sql && echo "Completed db restore"'
      }
    },

    useminPrepare: {
      options: {
        dest: 'dist'
      }
    },

    usemin: {
      options: {
        assetsDirs: 'dist',
      },
      html: ['dist/views/**/*.hbs'],
      css: ['dist/styles/**/*.css']
    },

    rev: {
      files: {
        src: [
            'dist/scripts/**/*.js',
            'dist/styles/**/*.css',
            'dist/img/**/*.{png,jpg,jpeg,svg,gif}',
        ]
      }
    }

  });

  // DEV TASKS ////////////////////////////

  grunt.registerTask('default', [
    'clean:dist',
    'env:dev',
    'concat:dev',
    'sass:dev',
    'express:dev',
    'watch'
  ]);

  grunt.registerTask('prod-build', [
    'clean:dist',
    'useminPrepare',
    'env:prod',
    'concat:prod',
    'sass:prod',
    'copy',
    'rev',
    'usemin'
  ]);
};